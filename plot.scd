(
(thisProcess.nowExecutingPath.dirname +/+ "make_taxonomy.scd").load;
~feedback = ~taxonomy.collect({ arg i; i[0];});
~faders = ~taxonomy.collect({ arg i; i[1];});
~faders = ~faders.dbamp;
~feedfaders = ~feedback.collect({ arg i, n; i.collect({ arg item, index; item * ~faders[n][index%8]})})
)

~scale = 15;

(
w = Window.new.front;
w.view.background_(Color.white);
w.drawFunc = {
	~feedfaders.do{
		arg item, i;
		item.do({
			arg value, index;
			Pen.color = Color.green(value, 0.25);
			Pen.addRect(
				//Rect((index%8)*~scale, (i*~scale*3)+((index/8).floor*~scale), ~scale, ~scale)
				Rect((index%6)*~scale, (i*~scale)+((index/6).floor*~scale), ~scale, ~scale)
			);
			Pen.perform([\stroke, \fill].choose);
		});

		//Pen.addRect(
		//	Rect(0, i*~scale*3, 200, 1)
		//);
	}
};
w.refresh;
)

(
//shrink matrix 60 6x97
var file;
var dirname = thisProcess.nowExecutingPath.dirname;

(dirname +/+ "make_taxonomy.scd").load;
~feedback = ~taxonomy.collect({ arg i; i[0];});
~faders = ~taxonomy.collect({ arg i; i[1];});
~faders = ~faders.dbamp;
~feedfaders = ~feedback.collect({ arg i, n; i.collect({ arg item, index; item * ~faders[n][index%8]})});

~feedfaders = ~feedfaders.round(0.0001);
~feedfaders.collect({
	arg item, i; item.size.postln;
	item.removeAt(8+15);item.removeAt(7+15);
	item.removeAt(8+7);item.removeAt(7+7);
	item.removeAt(8);item.removeAt(7);
	item.size.postln;
});

file = File(dirname ++ "/matrix.js".standardizePath,"w");
file.write("const matrix = [");
~feedfaders.collect({arg item; file.write(item.asString);file.write(",")});
file.write("]");
file.close;
)